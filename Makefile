# .PHONY:
# := разворачивает один раз при запуске и используется один раз на всю программу, константа
# =  вычисляет выражения $(MAKECMDGOALS) рекурсивно пока не откроет все

ROOT_DIR := ${CURDIR}
PATH_M4 := $(ROOT_DIR)/m4

args = $(filter-out $@,$(MAKECMDGOALS))
firstArgs = $(word 2,$(MAKECMDGOALS))
secondArgs = $(word 3,$(MAKECMDGOALS))
word-trunk = $(subst /, ,$(call firstArgs))

ENV = $(word 1, $(word-trunk))
SERVICE_NAME = $(word 2, $(word-trunk))
PATH_SRC := $(ROOT_DIR)/src

LOCAL_USER_ID=$(id -u)
LOCAL_GROUP_ID=$(id -g)

$(shell cat .env.example $(PATH_SRC)/.env > .env && echo -e "\nROOT_DIR=${CURDIR}\nLOCAL_USER_ID=$$(id -u)\nLOCAL_GROUP_ID=$$(id -g)" >> .env)

include .env
export

######### General #########

# %: - rule which match any task name; @: - empty recipe = do nothing
%:
	@:

.PHONY: help
help:                              ## Show this help
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' Makefile | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.PHONY: generate-folder
generate-folder:                           ## Create folder for production
	cd $(PATH_SRC)/$(call firstArgs) && [ -f ./run.sh ] && ./run.sh || true

.PHONY: env
env:
	@cat .env.example $(PATH_SRC)/.env > .env
	@echo -e "\nROOT_DIR=${CURDIR}\nLOCAL_USER_ID=$$(id -u)\nLOCAL_GROUP_ID=$$(id -g)" >> .env

######### Docker command #########
.PHONY: delete-container
delete-container:                  ## Delete docker container by name from args
	@docker rm -vf $(call firstArgs)

.PHONY: delete-image
delete-image:                      ## Delete docker image by name from args
	@docker rmi -f $(call firstArgs)

.PHONY: delete-all
delete-all:                        ## Delete all docker image and container
	@docker rm -vf $$(docker ps -a -q) 2>/dev/null || echo "No more containers to remove."
	@docker rmi -f $$(docker images -a -q) 2>/dev/null || echo "No more images to remove."

.PHONY: delete-unused
delete-unused:                     ## Delete empty docker image and container
	@docker ps -q -f status=exited | xargs --no-run-if-empty docker rm
	@docker images -q -f dangling=true | xargs --no-run-if-empty docker rmi
	@docker volume prune
	@docker system prune

######### COMPOSE #########
.PHONY: start
start:                           ## Generate DockerFile and start docker service
	@make compose-run $(call firstArgs) 'docker-compose up -d'

.PHONY: stop
stop:
	@cd $(PATH_SRC)/$(call firstArgs) && docker-compose down

.PHONY: push
push: --compose-dockerfile           ## Push docker container
	@docker login && cd $(PATH_SRC)/$(call firstArgs) && docker-compose -f build.yml push

.PHONY: debug
debug:                             ## Debug docker container
	@make compose-run $(call firstArgs) 'docker-compose up'

.PHONY: logs
logs:                             ## Show logs in docker container by name. ex: logs <service name>
	@cd $(PATH_SRC)/$(call firstArgs) && docker-compose logs

.PHONY: up
up:                               ## Up docker container
	docker-compose up

.PHONY: connect-local
connect-local:                        ## Connect to a raised instance docker container by name through shell using local uid:gid  ex: connect <service name>
	docker-compose exec -u ${LOCAL_USER_ID}:${LOCAL_GROUP_ID} $(call firstArgs) sh

.PHONY: connect-local-root
connect-local-root:                        ## Connect to a raised instance docker container by name through shell using local uid:gid  ex: connect <service name>
	docker-compose exec $(call firstArgs) sh

.PHONY: run
run:                               ## Connect docker container use current user and group
	cd $(PATH_SRC)/$(call firstArgs) && docker-compose run $(call firstArgs) sh

.PHONY: run-root
run-root:                         ## Connect docker container use root user and shell
	cd $(PATH_SRC)/$(call firstArgs) && docker-compose run --entrypoint=/bin/sh $(call firstArgs)

.PHONY: connect
connect:                        ## Connect to a raised instance docker container by name through shell using local uid:gid  ex: connect <service name>
	@cd $(PATH_SRC)/$(call firstArgs) && docker-compose exec -u ${LOCAL_USER_ID}:${LOCAL_GROUP_ID} $(call firstArgs) sh

.PHONY: connect-root
connect-root:                        ## Connect to a raised instance docker container by name through shell using local root ex: connect <service name>
	@cd $(PATH_SRC)/$(call firstArgs) && docker-compose exec $(call firstArgs) sh

.PHONY: build
build: --compose-dockerfile          ## Build docker container without cache
	@cd $(PATH_SRC)/$(call firstArgs) && docker-compose -f build.yml build --force-rm --no-cache

.PHONY: build-cache
build-cache: --compose-dockerfile    ## Build docker container with cache
	@cd $(PATH_SRC)/$(call firstArgs) && docker-compose -f build.yml build

.PHONY: --compose-dockerfile
--compose-dockerfile:
	[ -f $(PATH_SRC)/$(call firstArgs)/Dockerfile.m4 ] && cd $(PATH_SRC)/$(call firstArgs) && m4 -I $(PATH_M4) ./Dockerfile.m4 > Dockerfile | true

.PHONY: compose-run
compose-run:
	cd $(PATH_SRC)/$(call firstArgs) && [ -f ./run.sh ] && ./run.sh "$(wordlist 2, 1000, $(call args))" || $(wordlist 2, 1000, $(call args))

######## EACH CCOMMAND ########

.PHONY: each-all
each-all:                            ## Each all environment execute target (build, build-cache, etc).
ifeq ($(COMMAND),)
	@echo "Use:  make COMMAND=build each-all" && exit 1
endif
	@$(eval BUILD := nginx nginx-cert node-pm frontend php postgresql redis)
	@$(foreach var,$(BUILD), make $(COMMAND) $(var);)