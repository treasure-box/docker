builder(1) -- Management of image building
=============================================

## SYNOPSIS

`make <target> <args>` ...

## DESCRIPTION

**builder** is high level shell wrapper for management, build and testing image.

## TARGETS

These targets use for manager builder.

Basic

  * `build <name service>`:
    Build docker container without cache which are described build.yml file

  * `build-cache <name service>`:
    Build docker container with cache which are described build.yml file
    
  * `start <name service>`:
    Start docker service exec run.sh if exist

  * `push <name service>`:
    Push docker container in https://hub.docker.com/repositories

  * `up`:
    Up docker container are described in docker-compose.yml

  * `run <name service>`:
    Connect docker container use current user and group

  * `COMMAND="build" each-all`:
    For multiple any command execution (build, build-cache, etc).

Managing images

  * `delete-container`:
    Delete docker container by name from args

  * `delete-image`:
    Delete docker image by name from args

  * `delete-all`:
    Delete all docker image and container
    
  * `delete-unused`:
    Delete empty docker image and container unused

Helpers

  * `debug <name service>`:
    Debug docker container for up target

  * `generate-folder`:
    Create folder (log, container) for production enviroment

  * `help`:
    Show help described in Makefile
    
  * `run-root <name service>`:
    Connect docker container use root user and shell

## EXAMPLES

Executing the command **build** for all services registered in the src folder:

    $  make COMMAND=build each-all

Build docker container without cache which are described from `src/nginx/build.yml` file:

    $ make build nginx

Build docker container with cache which are described from `src/nginx/build.yml` file:

    $ make build-cache nginx

Start docker service wrapper run.sh from `src/nginx/docker-compose.yml` file:

    $ make start nginx
    
Push docker image on https://hub.docker.com/repositories use `src/nginx/docker-compose.yml` file:

    $ make push nginx
    
Up docker container are described in `docker-compose.yml`:

    $ make up
    
Connect docker container use current user and group:

    $ make run nginx

## ENVIRONMENT
  * `ENV`:
    The environment that is passed when running commands. Example: dev environment `make build dev/nginx`

  * `SERVICE_NAME`:
    The environment that is passed when running commands. Example: nginx service `make build dev/nginx`
    
  * `LOCAL_USER_ID`:
    The user to launch the container from
    
  * `LOCAL_GROUP_ID`:
    The group to launch the container from

## SEE ALSO
https://thewebland.net/development/devops/uskoryaem-rabotu-symfony-prilozheniya-v-docker/