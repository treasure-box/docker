
RUN buildDeps='coreutils su-exec>=0.2' HOME='/root' \
	&& set -x \
	&& apk add --update $buildDeps \
	&& apk add --no-cache --repository http://nl.alpinelinux.org/alpine/edge/community shadow \
	&& rm -rf /var/cache/apk/* \
;

RUN mkdir -p /scripts

RUN printf '#!/usr/bin/env sh\n\
HFOLDER=/home/docker\n\
IFILE=$HFOLDER/.initUser\n\
if [ ! -f "$IFILE" ]; then\n\
   \n\
    \n\
    \n\
    USER_ID=${LOCAL_USER_ID:-9001}\n\
    GROUP_ID=${LOCAL_GROUP_ID:-9001}\n\
    \n\
    mkdir -p $HFOLDER\n\
    \n\
    if [ $(getent group $GROUP_ID) ]; then\n\
         groupmod -n docker $(getent group $GROUP_ID | cut -d: -f1)\n\
    else\n\
         addgroup -g $GROUP_ID -S docker\n\
    fi\n\
    \n\
    if id $USER_ID >/dev/null 2>&1; then \n\
         deluser $(awk -v U=$USER_ID -F: '\''$3==U{print $1}'\'' /etc/passwd)\n\
    fi\n\
    adduser -u $USER_ID -D -S -h $HFOLDER -s /sbin/nologin -G docker docker\n\
    \n\
    chown -R $USER_ID:$GROUP_ID $HFOLDER\n\
    \n\
    touch $IFILE\n\
fi\n\
'\
>> /scripts/createUser.sh

RUN chmod u=rwx /scripts/createUser.sh