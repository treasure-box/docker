include(`macros.m4')
DONT_CHANGE(__file__)
FROM alpine

include(`su.m4')
RUN apk upgrade --no-cache && \
    apk add --update --no-cache curl postgresql-client redis && \
    rm -rf /var/cache/apk/*

WORKDIR /home/backup

COPY ./script/* /home/backup/script/
COPY ./crontab/* /home/backup/
RUN chmod a+x /home/backup/script/*.sh
RUN rm -rf /var/spool/cron/crontabs/root

COPY ./entrypoint.sh /
RUN chmod u=rwx /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]

CMD ["crond", "-f", "-l", "8"]