#!/bin/sh
# vim:sw=4:ts=4:et

set -e
source /scripts/createUser.sh

mkdir -p /home/backup && chown -R docker:docker /home/backup

if [ "$1" = 'crond' ]; then
    [ ! -f /home/backup/crontab ] && touch /home/backup/crontab

    mkdir -p /var/log/cron && touch /var/log/cron/cron.log && chown -R docker:docker /var/log/cron
    crontab -u docker /home/backup/crontab
    "$@"
else
    exec su-exec docker:docker "$@"
fi