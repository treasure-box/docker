#!/bin/sh

#Объявляем переменные
BACKUP_FOLDER=/home/backup

STORAGE_DIR=${STORAGE_DIR:-/home/backup/storage}
DATABASE_DIR=${STORAGE_DIR}/database

ARCHIVE_FOLDER=/
ARCHIVE_DIR=${BACKUP_FOLDER}/archive
CURRENT_ARCHIVE=backup_`date '+%d-%m-%Y'`
TYPE_ARCHIVE=.tar.gz

if ! [ -d $ARCHIVE_DIR ]
then
	mkdir -p $ARCHIVE_DIR
fi

if ! [ -d $DATABASE_DIR ]
then
	mkdir -p $DATABASE_DIR
fi

#Минимальная дата для удаления
MINDATE=`date '+%d-%m-%Y'`
#Приводим текущую дату к UNIX TIME (сколько секунд прошло от 01-01-1970 00:00:00)
CURE_UD=`date "+%s"`

date_str2sec()
{
DATE=`echo "$A"|cut -d'_' -f2 | cut -d '.' -f1`

D=`echo "$DATE"|cut -d'-' -f1`
M=`echo "$DATE"|cut -d'-' -f2`
Y=`echo "$DATE"|cut -d'-' -f3`

UD=`date "+%s" -d "$M/$D/$Y"`
}

echo "Размер директорий ${STORAGE_DIR} = `du -ch ${STORAGE_DIR} | tail -n1 |awk '{print $1}'`"

#Регулярное выражение на поиск формата даты времени года
LIST=`find $ARCHIVE_DIR -type f  | egrep -o 'backup_(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-](19|20)[0-9][0-9]'$TYPE_ARCHIVE`
COUNT=0
for A in $LIST; do
	date_str2sec $A

	if [ $CURE_UD -gt $UD ]
	then
		MINDATE=$DATE
		CURE_UD=$UD
	fi
	let COUNT=COUNT+1
done

STARTIME=`date "+%s"`

PGPASSWORD=${POSTGRES_PASSWORD} /usr/bin/pg_dump -U${POSTGRES_USER} -h postgres -d ${POSTGRES_DB} -C --column-inserts | gzip > ${DATABASE_DIR}/postresql.gz
/usr/bin/redis-cli -h redis --rdb ${DATABASE_DIR}/redis.rdb 2>/dev/null

tar -czpf ${ARCHIVE_DIR}/${CURRENT_ARCHIVE}${TYPE_ARCHIVE} ${STORAGE_DIR} 2>/dev/null

rm -R ${DATABASE_DIR}

ENDTIME=`date "+%s"`

TIME=0
let TIME=ENDTIME-STARTIME
echo "Архивация длилась: $((`date +%d --date=@$TIME -u`-1)) дней и `date +%H:%M:%S --date=@$TIME -u` (часов, минут, сек)"
echo "Свободное место на диске = `df -h $ARCHIVE_FOLDER |tail -n1|awk '{print $4}'`"
echo "Оставшееся свободное место на диске, где расположен архив = `df -h $ARCHIVE_DIR/ |tail -n1|awk '{print $4}'`"

if [[ $? -gt 0 ]]
then
	echo "[`date +%F--%H-%M`] Архивирование не удалось, появились ошибки, необходимо проверить"
	exit 1
else
	TestAr=`gzip -vt ${ARCHIVE_DIR}/${CURRENT_ARCHIVE}${TYPE_ARCHIVE} 2>&1 | cut -f2 | sed 's/^[ \t]*//'`
	if [ "$TestAr" == "" ]
	then
		if [ $COUNT -gt 3 ]
		then
			rm ${ARCHIVE_DIR}/backup_${MINDATE}${TYPE_ARCHIVE}
			echo "Старый архив backup_${MINDATE}${TYPE_ARCHIVE} был удалён."
		fi
	else
		echo "Тестирование архива ${ARCHIVE_DIR}/${CURRENT_ARCHIVE}${TYPE_ARCHIVE} завершилось не удачей, предыдущий backup не был удалён!"
	fi
fi

echo "Размер архива ${ARCHIVE_DIR}/${CURRENT_ARCHIVE}${TYPE_ARCHIVE} = [`du -h ${ARCHIVE_DIR}/${CURRENT_ARCHIVE}${TYPE_ARCHIVE} | tail -n1 |awk '{print $1}'`]"
