#!/bin/sh

ARCHIVE_DIR=/home/backup/archive
TYPE_ARCHIVE=.tar.gz

#Минимальная дата для удаления
MINDATE=`date '+%d-%m-%Y'`
#Приводим текущую дату к UNIX TIME (сколько секунд прошло от 01-01-1970 00:00:00)
CURE_UD=`date "+%s"`

date_str2sec()
{
DATE=`echo "$A"|cut -d'_' -f2 | cut -d '.' -f1`

D=`echo "$DATE"|cut -d'-' -f1`
M=`echo "$DATE"|cut -d'-' -f2`
Y=`echo "$DATE"|cut -d'-' -f3`

UD=`date "+%s" -d "$M/$D/$Y"`
}

#Регулярное выражение на поиск формата даты времени года
LIST=`find $ARCHIVE_DIR -type f  | egrep -o 'backup_(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-](19|20)[0-9][0-9]'$TYPE_ARCHIVE`
COUNT=0
for A in $LIST; do
	date_str2sec $A

	if [ $CURE_UD -gt $UD ]
	then
		MINDATE=$DATE
		CURE_UD=$UD
	fi
	let COUNT=COUNT+1
done

if [ $COUNT -gt 14 ]
then
	rm ${ARCHIVE_DIR}backup_${MINDATE}${TYPE_ARCHIVE}
fi
