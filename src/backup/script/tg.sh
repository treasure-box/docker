#!/bin/sh

[[ -z "$TG_CHAT_ID" || -z "$TG_TOKEN" ]] && { echo "Not set environment" ; exit 1; }

SEND_MSG=$1
EXEC_FILE="$(dirname "$0")"/$1.sh
if [ -f "$EXEC_FILE" ]; then
    SEND_MSG=`$EXEC_FILE`
fi

curl -d chat_id=${TG_CHAT_ID} -d text="$SEND_MSG" https://api.telegram.org/bot${TG_TOKEN}/sendMessage
