include(`macros.m4')
DONT_CHANGE(__file__)

FROM node:14.4-alpine3.11

ENV NODE_ENV=production

RUN apk add --update git openssh-client && \
    npm config set registry http://registry.npmjs.org/ && \
    rm -rf /var/cache/apk/* &&\
    mkdir -p /var/www/app

include(`su.m4')

WORKDIR /var/www/app

COPY ./config/known_hosts /home/docker/.ssh/known_hosts
COPY ./config/known_hosts /root/.ssh/known_hosts

COPY ./entrypoint.sh /
RUN chmod u=rwx /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]
