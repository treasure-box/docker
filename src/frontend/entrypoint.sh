#!/bin/sh
set -e
source /scripts/createUser.sh

chown docker:docker /dev/stdout /dev/stdin /dev/stderr
chown -R docker:docker /var/www/app

exec su-exec docker:docker "$@"
