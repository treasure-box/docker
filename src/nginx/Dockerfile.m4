include(`macros.m4')
DONT_CHANGE(__file__)
FROM nginx:1.19.2-alpine

include(`su.m4')

ENV NGINX_ROOT_DIR=/var/www/app \
    NGINX_PHP_HOST=php \
    DOMAINS=localhost

WORKDIR /etc/nginx

COPY ./entrypoint.sh /
RUN apk add --no-cache certbot openssl \
	&& rm -rf /var/cache/apk/* /docker-entrypoint.d/10-listen-on-ipv6-by-default.sh /docker-entrypoint.sh \
	&& mkdir -p /nginx/templates /etc/nginx/templates \
	&& chmod +x /entrypoint.sh

COPY ./config/etc ./
COPY ./config/template /nginx/templates
COPY ./config/docker-entrypoint.d /docker-entrypoint.d

EXPOSE 8080

ENTRYPOINT ["/entrypoint.sh"]
CMD ["nginx", "-g", "daemon off;"]