#!/bin/sh

ME=$(basename $0)

template=/nginx/templates/default.conf.template
if [[ -f "/nginx/templates/default.$MODE.conf.template" ]]; then
  template=/nginx/templates/default.$MODE.conf.template
fi

echo >&3 "$ME: Copy $template in /etc/nginx/templates/default.conf.template"
cp $template /etc/nginx/templates/default.conf.template


[ ! -d "$NGINX_ROOT_DIR" ] \
&& (echo >&3 "$ME: Create $NGINX_ROOT_DIR and touch index.html" \
&& mkdir -p "$NGINX_ROOT_DIR" \
&& echo 'Hello world!' > $NGINX_ROOT_DIR/index.html)

touch /var/run/nginx.pid /var/log/nginx/access.log /var/log/nginx/error.log
mkdir -p /var/cache/nginx