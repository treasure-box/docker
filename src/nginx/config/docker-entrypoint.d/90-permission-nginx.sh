#!/bin/sh

ME=$(basename $0)

echo >&3 "$ME: Set permission for nginx"

chown docker:docker /dev/stdout /dev/stdin /dev/stderr /var/run/nginx.pid
chown -R docker:docker /var/run /var/log/nginx /var/cache/nginx $NGINX_ROOT_DIR
