#!/bin/sh
set -e
source /scripts/createUser.sh

if [ "$1" = 'pm2-docker' ]; then
    chown docker:docker /dev/stdout /dev/stdin /dev/stderr
    chown -R docker:docker ~/
fi

exec su-exec docker:docker "$@"
