include(`macros.m4')
DONT_CHANGE(__file__)

FROM alpine:3.15

include(`su.m4')

ENV BUILD_DEPS="autoconf g++ make php7-dev php7-pear tzdata libmemcached-dev zlib-dev" \
    PHP_REDIS_VERSION=5.3.4 \
    PHP_SYSCONF_PATH=/etc/php7

# Installing necessary packages
RUN set -ex; \
    apk --update upgrade --no-cache; \
    apk add --no-cache --virtual .build-deps ${BUILD_DEPS}; \
    apk add --no-cache \
        git \
        curl \
        zlib \
        ca-certificates \
        openssl \
        php7 \
        php7-dom \
        php7-fpm \
        php7-cgi \
        php7-gd \
        php7-intl \
        php7-iconv \
        php7-mbstring \
        php7-mcrypt \
        php7-opcache \
        php7-pdo \
        php7-pdo_pgsql \
        php7-pdo_sqlite \
        php7-fileinfo \
        php7-xml \
        php7-simplexml \
        php7-xmlreader \
        php7-xmlwriter \
        php7-phar \
        php7-openssl \
        php7-json \
        php7-curl \
        php7-ctype \
        php7-session \
        php7-zlib \
        php7-tokenizer \
        php7-xdebug \
        php7-sodium \
        php7-posix \
# Additional
        php7-bcmath \
        php7-bz2 \
        php7-pcntl \
        php7-xsl \
        php7-zip; \
# Configuring timezones
    cp /usr/share/zoneinfo/Etc/UTC /etc/localtime; \
    echo "UTC" | tee /etc/timezone; \
# install PHP driver
    git clone --depth=1 -b ${PHP_REDIS_VERSION} https://github.com/phpredis/phpredis.git /tmp/phpredis && \
    cd /tmp/phpredis && phpize &&  ./configure &&  make && make install && \
    cd .. && rm -rf /tmp/phpredis && \
    echo 'extension=redis.so' >> ${PHP_SYSCONF_PATH}/conf.d/redis.ini \
    ; \
    rm -rf /tmp/pear; \
# Installing Composer
    curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer \
    && chmod +x /usr/local/bin/composer; \
# Cleaning
    apk del .build-deps; \
    rm -rf /var/cache/apk/*;

WORKDIR /var/www/app

RUN mkdir -p /var/log/php /run/php /var/log/xdebug

EXPOSE 9000 9001

ENV LC_ALL=en_US.UTF-8 \
    LANG=en_US.UTF-8 \
# XDebug configs
    XDEBUG_CLIENT_PORT=9000,9001,9003 \
    XDEBUG_CLIENT_HOST=host.docker.internal \
    XDEBUG_IDE_KEY=PHPSTORM \
    XDEBUG_LOG=/var/log/xdebug/xdebug.log \
# php-fpm.conf
    PHP_FPM_ERROR_LOG=/dev/stderr \
# php.ini
    PHP_MEMORY_LIMIT=256M \
    PHP_PRECISION=-1 \
    PHP_ERROR_REPORTING="E_ALL & ~E_DEPRECATED & ~E_STRICT" \
    PHP_OUTPUT_BUFFERING=4096 \
    PHP_SERIALIZE_PRECISION=-1 \
    PHP_MAX_EXECUTION_TIME=30 \
    PHP_DISPLAY_ERRORS=off \
    PHP_DISPLAY_STARTUP_ERRORS=Off \
    PHP_POST_MAX_SIZE=20M \
    PHP_MAX_UPLOAD_FILESIZE=10M \
    PHP_MAX_FILE_UPLOADS=20 \
    PHP_MAX_INPUT_TIME=60 \
    PHP_VARIABLES_ORDER=GPCS \
    PHP_REQUEST_ORDER=GP \
    PHP_OPCACHE_ENABLE=1 \
    PHP_OPCACHE_ENABLE_CLI=0 \
    PHP_OPCACHE_MEMORY_CONSUMPTION=128 \
    PHP_OPCACHE_INTERNED_STRINGS_BUFFER=32 \
    PHP_OPCACHE_MAX_ACCELERATED_FILES=10000 \
    PHP_OPCACHE_USE_CWD=1 \
    PHP_OPCACHE_VALIDATE_TIMESTAMPS=1 \
    PHP_OPCACHE_REVALIDATE_FREQ=2 \
    PHP_OPCACHE_ENABLE_FILE_OVERRIDE=0 \
    PHP_ZEND_ASSERTIONS=-1 \
    PHP_ZEND_EXCEPTION_IGNORE_ARGS=on \
    PHP_DISABLE_FUNCTIONS=pcntl_alarm,pcntl_fork,pcntl_waitpid,pcntl_wait,pcntl_wifexited,pcntl_wifstopped,pcntl_wifsignaled,pcntl_wifcontinued,pcntl_wexitstatus,pcntl_wtermsig,pcntl_wstopsig,pcntl_signal,pcntl_signal_dispatch,pcntl_get_last_error,pcntl_strerror,pcntl_sigprocmask,pcntl_sigwaitinfo,pcntl_sigtimedwait,pcntl_exec,pcntl_getpriority,pcntl_setpriority \
# www.conf
    WWW_PHP_PM=dynamic \
    WWW_PHP_PM_MAX_CHILDREN=5 \
    WWW_PHP_PM_START_SERVERS=2 \
    WWW_PHP_PM_MIN_SPARE_SERVERS=1 \
    WWW_PHP_PM_MAX_SPARE_SERVERS=3 \
    WWW_PHP_PM_PROCESS_IDLE_TIMEOUT=10s \
    WWW_PHP_PM_MAX_REQUESTS=500 \
# App settings
    CRONTAB_EXEC=false

COPY ./config/php-fpm.conf /etc/php7/php-fpm.conf
COPY ./config/php.ini /etc/php7/php.ini
COPY ./config/www.conf /etc/php7/php-fpm.d/www.conf
COPY ./config/xdebug.ini /etc/php7/conf.d/xdebug.ini

COPY ./entrypoint.sh /
RUN chmod u=rwx /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]

CMD ["php-fpm7", "--nodaemonize"]