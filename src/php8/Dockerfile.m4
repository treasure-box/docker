include(`macros.m4')
DONT_CHANGE(__file__)

FROM php:8.3.17-fpm-alpine3.20

include(`su.m4')

ENV BUILD_DEPS="autoconf build-base linux-headers zlib libzip bzip2 libpng libjpeg freetype" \
    PHP_REDIS_VERSION=5.3.7 \
    PHP_XDEBUG_VERSION=3.4.1 \
    PHP_SYSCONF_PATH=/usr/local/etc/php

# Installing necessary packages
RUN set -ex; \
    apk --update upgrade --no-cache; \
    apk add --no-cache --virtual .build-deps ${BUILD_DEPS}; \
    apk add --no-cache \
        libevent-dev \
        zlib-dev \
        libzip-dev \
        openssl-dev \
        icu-dev \
        libwebp-dev \
        freetype-dev \
        postgresql-dev \
        libjpeg-turbo-dev \
        libpng-dev \
        git \
        curl \
        bash \
        wget; \
## Configuring timezones
#    cp /usr/share/zoneinfo/Etc/UTC /etc/localtime; \
#    echo "UTC" | tee /etc/timezone; \
# Install PHP extensions
    docker-php-ext-configure gd --with-freetype=/usr/include/ --with-jpeg=/usr/include/; \
    docker-php-ext-install -j$(getconf _NPROCESSORS_ONLN) \
    intl \
    gd \
    bcmath \
    pcntl \
    pdo_pgsql \
    sockets \
    zip

RUN pecl channel-update pecl.php.net \
    && pecl install -o -f \
        redis-${PHP_REDIS_VERSION} \
        xdebug-${PHP_XDEBUG_VERSION} \
    && rm -rf /tmp/pear \
    && docker-php-ext-enable redis \
    && docker-php-ext-enable xdebug

# Installing Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer \
    && chmod +x /usr/local/bin/composer; \
# Installing Symfony-cli
    curl -1sLf 'https://dl.cloudsmith.io/public/symfony/stable/setup.alpine.sh' | bash; \
    apk add symfony-cli; \
# Cleaning
    apk del .build-deps; \
    rm -rf /var/cache/apk/*;

ENV LC_ALL=en_US.UTF-8 \
    LANG=en_US.UTF-8 \
# XDebug configs
    XDEBUG_CLIENT_PORT=9000,9001,9003 \
    XDEBUG_CLIENT_HOST=host.docker.internal \
    XDEBUG_IDE_KEY=PHPSTORM \
    XDEBUG_MODE=debug,develop \
    XDEBUG_LOG=/var/log/xdebug/xdebug.log \
    XDEBUG_LOG_LEVEL=0 \
# php-fpm.conf
    PHP_FPM_ERROR_LOG=/dev/stderr \
# php.ini
    PHP_MEMORY_LIMIT=256M \
    PHP_PRECISION=-1 \
    PHP_ERROR_REPORTING="E_ALL & ~E_DEPRECATED & ~E_STRICT" \
    PHP_OUTPUT_BUFFERING=4096 \
    PHP_SERIALIZE_PRECISION=-1 \
    PHP_MAX_EXECUTION_TIME=30 \
    PHP_DISPLAY_ERRORS=off \
    PHP_DISPLAY_STARTUP_ERRORS=Off \
    PHP_POST_MAX_SIZE=20M \
    PHP_MAX_UPLOAD_FILESIZE=10M \
    PHP_MAX_FILE_UPLOADS=20 \
    PHP_MAX_INPUT_TIME=60 \
    PHP_VARIABLES_ORDER=GPCS \
    PHP_REQUEST_ORDER=GP \
    PHP_OPCACHE_ENABLE=1 \
    PHP_OPCACHE_ENABLE_CLI=0 \
    PHP_OPCACHE_MEMORY_CONSUMPTION=128 \
    PHP_OPCACHE_INTERNED_STRINGS_BUFFER=32 \
    PHP_OPCACHE_MAX_ACCELERATED_FILES=10000 \
    PHP_OPCACHE_USE_CWD=1 \
    PHP_OPCACHE_VALIDATE_TIMESTAMPS=1 \
    PHP_OPCACHE_REVALIDATE_FREQ=2 \
    PHP_OPCACHE_ENABLE_FILE_OVERRIDE=0 \
    PHP_ZEND_ASSERTIONS=-1 \
    PHP_DISABLE_FUNCTIONS=pcntl_alarm,pcntl_fork,pcntl_waitpid,pcntl_wait,pcntl_wifexited,pcntl_wifstopped,pcntl_wifsignaled,pcntl_wifcontinued,pcntl_wexitstatus,pcntl_wtermsig,pcntl_wstopsig,pcntl_signal,pcntl_signal_dispatch,pcntl_get_last_error,pcntl_strerror,pcntl_sigprocmask,pcntl_sigwaitinfo,pcntl_sigtimedwait,pcntl_exec,pcntl_getpriority,pcntl_setpriority \
# www.conf
    PHP_PM=dynamic \
    PHP_PM_MAX_CHILDREN=5 \
    PHP_PM_START_SERVERS=2 \
    PHP_PM_MIN_SPARE_SERVERS=1 \
    PHP_PM_MAX_SPARE_SERVERS=3 \
    PHP_PM_PROCESS_IDLE_TIMEOUT=10s \
    PHP_PM_MAX_REQUESTS=500 \
# App settings
    CRONTAB_EXEC=false

WORKDIR /var/www/app

RUN mkdir -p /var/log/php /run/php /var/log/xdebug

EXPOSE 9000 9001

COPY ./config/php-fpm.conf ${PHP_SYSCONF_PATH}/php-fpm.conf
COPY ./config/php.ini ${PHP_SYSCONF_PATH}/php.ini
COPY ./config/www.conf ${PHP_SYSCONF_PATH}/php-fpm.d/www.conf
COPY ./config/xdebug.ini ${PHP_SYSCONF_PATH}/conf.d/docker-php-ext-xdebug.ini

COPY ./entrypoint.sh /
RUN chmod u=rwx /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]

CMD ["php-fpm", "--nodaemonize"]