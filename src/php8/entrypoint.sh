#!/usr/bin/env sh
set -e
source /scripts/createUser.sh

if [ "$1" = 'php-fpm' ]; then

    [ ! -f /var/log/php/error.log ] && touch /var/log/php/error.log

    chown docker:docker /dev/stdout /dev/stdin /dev/stderr
    chown -R docker:docker /var/log/php /run/php /var/www/app
fi

if [[ -f "/etc/php7/conf.d/xdebug.ini" ]]; then
  [ ! -f /var/log/xdebug/xdebug.log ] && touch /var/log/xdebug/xdebug.log
  chown -R docker:docker /var/log/xdebug
fi

if [ -f "/var/www/app/crontab" ] && [ "${CRONTAB_EXEC}" == "true" ]; then
  mkdir -p /var/log/cron && touch /var/log/cron/cron.log && chown -R docker:docker /var/log/cron
  crontab -u docker /var/www/app/crontab
  su-exec docker:docker crond -l 8
fi

exec su-exec docker:docker "$@"
