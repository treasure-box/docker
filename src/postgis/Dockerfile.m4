include(`macros.m4')
DONT_CHANGE(__file__)
FROM postgis/postgis:16-3.4-alpine

include(`su.m4')
COPY ./config/etc/* /var/lib/postgresql/data/
COPY ./entrypoint.sh /
RUN chmod u=rwx /entrypoint.sh

EXPOSE 5432
ENTRYPOINT ["/entrypoint.sh"]
CMD ["postgres", "-c", "config_file=/var/lib/postgresql/data/postgresql.conf"]
