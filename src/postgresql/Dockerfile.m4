include(`macros.m4')
DONT_CHANGE(__file__)
FROM postgres:16.0-alpine3.18

include(`su.m4')
COPY ./config/etc/* /etc/postgresql/
COPY ./entrypoint.sh /
RUN chmod u=rwx /entrypoint.sh

EXPOSE 5432
ENTRYPOINT ["/entrypoint.sh"]
CMD ["postgres", "-c", "config_file=/var/lib/postgresql/data/postgresql.conf"]
