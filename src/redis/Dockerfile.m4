include(`macros.m4')
DONT_CHANGE(__file__)
FROM redis:3.2.11-alpine

include(`su.m4')

WORKDIR /data

COPY ./config/etc/redis.conf /etc/redis/redis.conf
COPY ./entrypoint.sh /
RUN chmod u=rwx /entrypoint.sh
EXPOSE 6379

ENTRYPOINT ["/entrypoint.sh"]
CMD ["redis-server", "/etc/redis/redis.conf"]