#!/bin/sh
set -e
source /scripts/createUser.sh

if [ "$1" = 'redis-server' ]; then

    mkdir -p /var/log/redis
    [ ! -f /var/log/redis/redis.log ] && touch /var/log/redis/redis.log

    chown docker:docker /dev/stdout /dev/stdin /dev/stderr
    chown -R docker:docker /data /var/log/redis/redis.log
fi

exec su-exec docker:docker "$@"