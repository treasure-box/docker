#!/usr/bin/env bash

if [ ! -f /etc/sysctl.d/redis.conf ]; then
    echo never | sudo tee /sys/kernel/mm/transparent_hugepage/enabled
    echo never | sudo tee /sys/kernel/mm/transparent_hugepage/defrag
    echo "vm.overcommit_memory=1" | sudo tee /etc/sysctl.d/redis.conf
    echo "net.core.somaxconn=65535" | sudo tee -a /etc/sysctl.d/redis.conf
    sudo sysctl --system
fi

mkdir -p ${LOG_FOLDER}/redis
mkdir -p ${CONTAINER_FOLDER}/redis/data

$@